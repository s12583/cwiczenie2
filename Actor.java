package com.tv_class;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Actor
{

    private int ID_TVSERIES;
    private int ID;
    private String name;
    private Date dateofBirth;
    private String biography;

    public Actor(int id, String name, String dateofBirth, String biography, TvSeries tv)
    {

        try
        {
            this.ID_TVSERIES = tv.getId();
            this.ID = id;
            this.name = name;
            this.dateofBirth = new SimpleDateFormat("yyyy-MM-dd").parse(dateofBirth);
            
        } catch (ParseException ex)
        {
            Logger.getLogger(Actor.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.biography = biography;

    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getDateofBirth()
    {
        return dateofBirth;
    }

    public void setDateofBirth(Date dateofBirth)
    {
        this.dateofBirth = dateofBirth;
    }

    public String getBiography()
    {
        return biography;
    }

    public void setBiography(String biography)
    {
        this.biography = biography;
    }

    public int getID_TVSERIES()
    {
        return ID_TVSERIES;
    }

    public void setID_TVSERIES(int ID_TVSERIES)
    {
        this.ID_TVSERIES = ID_TVSERIES;
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

}
