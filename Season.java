package com.tv_class;

import java.util.ArrayList;
import java.util.List;

public class Season
{

    private int ID;
    private int seasonNumber;
    private int yearOfRealease;
    private List<Episode> episodes;
    private int IDTVSERIES;

    public Season(int id, int seasonNumber, int yearOfRealease, List<Episode> ep)
    {
        this.episodes = new ArrayList();

        this.ID = id;
        this.seasonNumber = seasonNumber;
        this.yearOfRealease = yearOfRealease;

        ep.forEach((e) ->{this.episodes.add(e);});

        this.setIdEpisode();

        //this.IDTVSERIES = tvseries.getId();//-sprawdzenie 
    }

    private void setIdEpisode()
    {
        this.episodes.forEach(a -> a.setIDSEASON(ID));

    }

    public int getSeasonNumber()
    {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber)
    {
        this.seasonNumber = seasonNumber;
    }

    public int getYearOfRealease()
    {
        return yearOfRealease;
    }

    public void setYearOfRealease(int yearOfRealease)
    {
        this.yearOfRealease = yearOfRealease;
    }

    public List<Episode> getEpisodes()
    {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes)
    {
        this.episodes = episodes;
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public int getIDTVSERIES()
    {
        return IDTVSERIES;
    }

    public void setIDTVSERIES(int IDTVSERIES)
    {
        this.IDTVSERIES = IDTVSERIES;
    }

}
